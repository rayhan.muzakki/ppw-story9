from django.test import Client, TestCase, LiveServerTestCase
from selenium import webdriver
import time

# Create your tests here.
class unitTest(TestCase):

    def test_idxurl(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_indextemplate(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')

class functionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')


    def tearDown(self):
        self.driver.quit()
    
    def test_searchBook(self):
        self.driver.get(self.live_server_url)
        submit_box=self.driver.find_element_by_id("search")
        submit_box.send_keys("sierokarte")
        self.driver.find_element_by_id("submit").click()
        time.sleep(3)
        title_name= self.driver.find_elements_by_id("title0")
        self.assertIn('Granblue Fantasy',title_name[0].text)
        
    def test_incremenetLike(self):
        self.driver.get(self.live_server_url)
        submit_box=self.driver.find_element_by_id("search")
        submit_box.send_keys("harry potter")
        self.driver.find_element_by_id("submit").click()
        time.sleep(3)
        self.driver.find_elements_by_id("button0")[0].click()
        like_incremenet= self.driver.find_elements_by_id("likes0")
        self.assertEqual('1',like_incremenet[0].text)
        
    def test_topBooks(self):
        self.driver.get(self.live_server_url)
        submit_box=self.driver.find_element_by_id("search")
        submit_box.send_keys("harry potter")
        self.driver.find_element_by_id("submit").click()
        time.sleep(3)
        self.driver.find_elements_by_id("button0")[0].click()
        self.driver.find_elements_by_id("button1")[0].click()
        self.driver.find_elements_by_id("button1")[0].click()
        time.sleep(3)
        self.driver.find_elements_by_id("modal_button")[0].click()
        time.sleep(3)
        top_one= self.driver.find_elements_by_id("top0")
        title_nameone= self.driver.find_elements_by_id('title1')[0].text
        self.assertIn('1. '+title_nameone,top_one[0].text)
        top_two= self.driver.find_elements_by_id("top1")
        title_nametwo= self.driver.find_elements_by_id('title0')[0].text
        self.assertIn('2. '+title_nametwo,top_two[0].text)
                       